import JobBase from "./JobBase";
import {getPriorityFromBuildingType} from "organization/BuildingPlanner";
import {BehaviorReturnCode} from "behaviors/BehaviorBase";
import {MoveToAndPerformActionBehavior} from "behaviors/MoveToAndPerformActionBehavior";

export class RepairStructureBehavior extends MoveToAndPerformActionBehavior<Structure>
{
	constructor(target: Structure, requestResupply = true)
	{
		super(target, Creep.prototype.repair);
	}

	handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode
	{
		switch(returnCode as CreepActionReturnCode | ERR_NOT_ENOUGH_ENERGY)
		{
			case ERR_NOT_OWNER:
			case ERR_NOT_ENOUGH_ENERGY:
				return ERR_INVALID_TARGET;
			default: throw new Error(`Unhandled return code: ${returnCode}`);
		}
	}
}

export default class RepairStructureJob extends JobBase
{
	structureId: Id<Structure>;
	constructor(structure: Structure)
	{
		super(
			"RepairStructure",
			{
				maxAssigned: 1,
				atom: structure.id,
				priority: getPriorityFromBuildingType(structure.structureType),
				location: structure.pos,
				behavior: new RepairStructureBehavior(structure),
			}
		);
		this.structureId = structure.id;
	}

	run(): boolean
	{
		const structure = Game.getObjectById(this.structureId);
		return structure !== null && structure.hits < structure.hitsMax;
	}
}