import {getAllJobAssignable, getLogLevelFromJobPriority, log, LogLevel} from "utils/misc";
import JobBase from "./JobBase";
import JobPriority, {getJobPrioritiesSorted} from "./JobPriority";

export default class JobQueue
{
	nextJobIndex = 0;
	jobs: JobBase[] = [];

	addJob(job: JobBase): void
	{
		const previousExists = this.jobs.find(f => job.atomEquals(f));
		if(previousExists)
		{
			previousExists.resetTimeout();
		}
		else
		{
			this.jobs.push(job);
			log(job.priority > JobPriority.MAINTAIN ? LogLevel.INFO : LogLevel.WALL, "JOB ADD", job.toString());
		}
	}

	removeJob(jobToRemove: JobBase): boolean
	{
		const prevLength = this.jobs.length;
		this.jobs = this.jobs.filter(job => job.atom !== jobToRemove.atom);
		const retVal = this.jobs.length < prevLength;
		if(retVal)
		{
			const creep = Game.creeps[jobToRemove.assigned[0] || "None"];
			log(LogLevel.WALL, "JOB REMOVE", jobToRemove.toString(), creep);
		}
		return retVal;
	}

	getJobById(id?: Id<JobBase> | null): JobBase | null
	{
		if(id === null || id === undefined){return null;}
		return this.jobs.reduce((prev, next) => next.id === id ? next : prev, null as JobBase | null);
	}

	flushJobs(): void
	{
		for(const index in this.jobs)
		{
			delete this.jobs[index];
		}
		this.jobs = [];
		for(const creep of getAllJobAssignable())
		{
			creep.memory.assignedJob = null;
		}
	}

	run(): JobBase[]
	{
		const prioritiesInOrder = getJobPrioritiesSorted();
		const fillableJobsByPriority: JobBase[][] = [];
		for(const prio of prioritiesInOrder){fillableJobsByPriority[prio] = [];}
		this.jobs = this.jobs.filter(job =>
		{
			let retVal = true;
			const creep = Game.creeps[job.assigned[0] || "None"];
			if((Game.time - job.creationTime) > job.timeout)
			{
				retVal = false;
				log(getLogLevelFromJobPriority(job.priority), "JOB EXPIRED", `${job.toString()}:${job.timeout}t`, creep);
			}
			else if(!job.run())
			{
				retVal = false;
				log(getLogLevelFromJobPriority(job.priority), "JOB DONE", job.toString(), creep);
			}
			else if(job.capacity())
			{
				fillableJobsByPriority[job.priority].push(job);
			}
			if(!retVal){job.unassignAll();}
			return retVal;
		});
		const fillableJobs = [];
		for(const prio of prioritiesInOrder)
		{
			fillableJobs.push(...fillableJobsByPriority[prio]);
		}
		return fillableJobs;
	}

	attemptFillJob(ent: JobAssignable, job: JobBase | null): boolean
	{
		const memory = ent.memory;
		const role = global.roleIndex.getRole(ent);
		if(job != null && job.capacity() && !memory.assignedJob && role.canAcceptJob(ent, job))
		{
			job.assignJob(ent);
			return true;
		}
		return false;
	}
}