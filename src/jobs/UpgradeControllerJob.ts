import {BehaviorReturnCode} from "behaviors/BehaviorBase";
import {MoveToAndPerformActionBehavior} from "behaviors/MoveToAndPerformActionBehavior";
import {log, LogLevel} from "utils/misc";
import JobBase from "./JobBase";
import JobPriority from "./JobPriority";

export class UpgradeControllerBehavior extends MoveToAndPerformActionBehavior<StructureController>
{
	constructor(target: StructureController, requestResupply = true)
	{
		super(target, Creep.prototype.upgradeController);
	}

	handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode
	{
		switch(returnCode)
		{
			case ERR_NOT_OWNER:
			case ERR_NOT_ENOUGH_RESOURCES:
			case ERR_INVALID_TARGET:
				return ERR_INVALID_TARGET;
			default: throw new Error(`Unhandled return code: ${returnCode}`);
		}
	}
}

export default class UpgradeControllerJob extends JobBase
{
	controllerId: Id<StructureController>;
	constructor(controller: StructureController)
	{
		super("UpgradeController",
			{
				maxAssigned: 1,
				atom: controller.id,
				priority: JobPriority.TIMESINK,
				location: controller.pos,
				behavior: new UpgradeControllerBehavior(controller),
			});
		this.controllerId = controller.id;
	}

	run(): boolean
	{
		const controller = Game.getObjectById(this.controllerId) as StructureController;
		if(controller.ticksToDowngrade <= 3000)
		{
			log(LogLevel.DANGER, "LOSTCONTROL", `Controller in ${controller.room.name} is decaying!`);
			this.priority = JobPriority.DANGER;
		}
		else{this.priority = JobPriority.EXPAND;}
		return UpgradeControllerJob.isJobNeeded(controller);
	}

	static isJobNeeded(controller: StructureController | null): boolean
	{
		return controller !== null && !controller.upgradeBlocked && (controller.level < 8 || controller.ticksToDowngrade < 5000);
	}

	toString(): string
	{
		const structure = Game.getObjectById(this.controllerId) as StructureController;
		return `${super.toString()}:${structure?.room?.name || "???"}`;
	}
}