import {BehaviorReturnCode} from "behaviors/BehaviorBase";
import {MoveToAndPerformActionBehavior} from "behaviors/MoveToAndPerformActionBehavior";
import EnergySupplyDestination, {getEnergySupplyCapacity} from "types/EnergySupplyDestination";
import {TransferReturnCode} from "types/TransferReturnCode";
import JobBase, {JobBaseConfigurator} from "./JobBase";
import JobPriority from "./JobPriority";

export const SUPPLY_FULL_TIMEOUT = 1;

function supplyEnergy(this: Creep, target: EnergySupplyDestination): TransferReturnCode
{
	const capacity = getEnergySupplyCapacity(target);
	if(capacity === 0){return ERR_FULL;}
	const carrying = this.store.energy;
	if(carrying === 0){return ERR_NOT_ENOUGH_ENERGY;}
	return this.transfer(target, RESOURCE_ENERGY, Math.min(capacity, carrying)) as TransferReturnCode;
}

export class SupplyEnergyBehavior extends MoveToAndPerformActionBehavior<EnergySupplyDestination>
{
	constructor(target: EnergySupplyDestination)
	{
		super(target, supplyEnergy);
	}

	handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode
	{
		switch(returnCode as TransferReturnCode)
		{
			case ERR_FULL: return ERR_BUSY;
			case ERR_NOT_ENOUGH_ENERGY: return ERR_INVALID_TARGET;
			default: throw new Error(`Unhandled return code: ${returnCode}`);
		}
	}
}

export default class SupplyEnergyJob extends JobBase
{
	destId: Id<EnergySupplyDestination>;
	fullTime: number | null = null;
	constructor(dest: EnergySupplyDestination, extraOpts?: Partial<JobBaseConfigurator>)
	{
		super("SupplyEnergy",
			{
				maxAssigned: 1,
				atom: dest.id,
				priority: SupplyEnergyJob.getDefaultJobPriority(dest),
				location: dest.pos,
				behavior: new SupplyEnergyBehavior(dest),
				...extraOpts,
			});
		this.destId = dest.id;
	}

	static getDefaultJobPriority(dest: EnergySupplyDestination): JobPriority
	{
		const possibleStructure = dest as StructureWithStore;
		const{structureType} = possibleStructure;
		if(!structureType){return JobPriority.EXPAND;}
		switch(structureType)
		{
			case STRUCTURE_CONTROLLER:
				return JobPriority.TIMESINK;
			case STRUCTURE_EXTENSION:
			case STRUCTURE_SPAWN:
				return JobPriority.MAINTAIN;
			case STRUCTURE_TOWER:
				return JobPriority.DANGER;
			default:
				return JobPriority.EXPAND;
		}
	}

	run(): boolean
	{
		const dest = Game.getObjectById(this.destId);
		if(SupplyEnergyJob.isJobNeeded(dest))
		{
			this.priority = SupplyEnergyJob.getDefaultJobPriority(dest as EnergySupplyDestination);
			this.fullTime = null;
			return true;
		}
		else
		{
			this.priority = JobPriority.TIMESINK;
			if(this.fullTime === null){this.fullTime = Game.time;}
			return (Game.time - this.fullTime) < SUPPLY_FULL_TIMEOUT;
		}
	}

	static isJobNeeded(dest: EnergySupplyDestination | null): boolean
	{
		return dest !== null && getEnergySupplyCapacity(dest) > 0;
	}

	toString(): string
	{
		const dest = Game.getObjectById(this.destId) as any;
		return `${super.toString()}:${(dest?.structureType ?? dest?.memory?.role ?? "???") as string}`;
	}
}