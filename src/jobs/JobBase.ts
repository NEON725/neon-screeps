import BehaviorBase, {NotImplementedBehavior} from "behaviors/BehaviorBase";
import {deepEquals, generateID, getJobAssignableByName, log, LogLevel, tryCastAsJobAssignable} from "utils/misc";
import JobPriority from "./JobPriority";

const DEFAULT_JOB_TIMEOUT = 60;

export interface JobBaseConfigurator
{
	maxAssigned?: number,
	atom: any,
	priority?: JobPriority,
	location: RoomPosition,
	timeout?: number,
	behavior?: BehaviorBase,
}

export default abstract class JobBase
{
	id: Id<JobBase>;
	maxAssigned;
	atom: any;
	priority: JobPriority;
	assigned: string[] = [];
	location: RoomPosition;
	creationTime: number;
	timeout: number;
	behavior: BehaviorBase;
	constructor(public jobName: string, configurator: JobBaseConfigurator)
	{
		this.id = generateID();
		this.maxAssigned = configurator.maxAssigned ?? 1;
		this.atom = configurator.atom;
		this.priority = configurator.priority ?? JobPriority.TIMESINK;
		this.location = configurator.location;
		this.creationTime = Game.time;
		this.timeout = configurator.timeout ?? DEFAULT_JOB_TIMEOUT;
		this.behavior = configurator.behavior ?? new NotImplementedBehavior();
	}

	atomEquals(other: JobBase): boolean
	{
		return !!(this.atom && other.atom && (this.atom === other.atom || deepEquals(this.atom, other.atom)));
	}

	assignJob(creep: JobAssignable): void
	{
		log(LogLevel.WALL, "JOB ASSIGN", this.toString(), creep);
		creep.memory.assignedJob = this.id;
		this.assigned.push(creep.name);
	}

	isAssignedTo(creep: JobAssignable): boolean
	{
		return this.assigned.includes(creep.name);
	}

	capacity(): number
	{
		return this.maxAssigned - this.assigned.length;
	}

	unassignJob(creepOrName: JobAssignable | string): void
	{
		const name = (creepOrName as JobAssignable)?.name ?? creepOrName;
		const creep = tryCastAsJobAssignable(creepOrName) ?? getJobAssignableByName(name);
		log(LogLevel.WALL, "JOB UNASSIGN", this.toString(), creep);
		if(creep){creep.memory.assignedJob = null;}
		this.assigned = this.assigned.filter(remove => remove !== name);
	}

	unassignAll(): void
	{
		for(const name of this.assigned)
		{
			this.unassignJob(name);
		}
		this.assigned = [];
	}

	abstract run(): boolean;

	getDefaultBehavior(): BehaviorBase
	{
		return this.behavior;
	}

	resetTimeout(timeout?: number): void
	{
		this.creationTime = Game.time;
		this.timeout = timeout ?? this.timeout;
	}

	toString(): string
	{
		return `${this.jobName}:${JSON.stringify(this.atom)}:${JobPriority[this.priority]}`;
	}
}