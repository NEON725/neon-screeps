import AnyStockpile, {getStockpiledResourceAmount} from "types/AnyStockpile";
import JobBase from "./JobBase";
import JobPriority from "./JobPriority";

export default class EmptyStockpileJob extends JobBase
{
	containerId: Id<AnyStockpile>;
	constructor(container: AnyStockpile, public resource: ResourceConstant)
	{
		super("EmptyStockpile",
			{
				maxAssigned: 2,
				atom: container.id,
				priority: JobPriority.TIMESINK,
				location: container.pos,
				timeout: Infinity,
			});
		this.containerId = container.id;
	}

	run(): boolean
	{
		return EmptyStockpileJob.isJobNeeded(Game.getObjectById(this.containerId), this.resource);
	}

	static isJobNeeded(container: AnyStockpile | null, resource: ResourceConstant): boolean
	{
		return container !== null && getStockpiledResourceAmount(container, resource) > 0;
	}
}