import BehaviorBase, {BehaviorReturnCode} from "behaviors/BehaviorBase";
import {MoveToAndPerformActionBehavior} from "behaviors/MoveToAndPerformActionBehavior";
import {generateCompareDistanceFrom} from "utils/misc";
import {ConstructBuildingBehavior} from "./ConstructBuildingJob";
import JobBase from "./JobBase";
import JobPriority from "./JobPriority";
import {RepairStructureBehavior} from "./RepairStructureJob";
import {SupplyEnergyBehavior} from "./SupplyEnergyJob";

export type ResourceVein = Source | Mineral | Deposit;
export const RESOURCE_RETURN_STRUCTURES: StructureConstant[] = [STRUCTURE_CONTAINER, STRUCTURE_STORAGE];

type DropoffBehavior = MoveToAndPerformActionBehavior<ConstructionSite | Structure | AnyCreep>;

export class ReturnResourceBehavior extends BehaviorBase
{
	private dropoffBehavior: DropoffBehavior | null;
	constructor(private resource: ResourceConstant)
	{
		super();
		this.dropoffBehavior = null;
	}

	run(self: AnyCreep): BehaviorReturnCode
	{
		if(this.dropoffBehavior)
		{
			const result = this.dropoffBehavior.run(self);
			switch(result)
			{
				case ERR_INVALID_TARGET: break;
				default: return result;
			}
			this.dropoffBehavior = null;
		}
		const compareFn = generateCompareDistanceFrom(self);
		const dropoff: StructureWithStore | ConstructionSite | AnyCreep | undefined
			= self.pos.findInRange(FIND_CONSTRUCTION_SITES, 3)
				.find(s => RESOURCE_RETURN_STRUCTURES.includes(s.structureType))
			?? self.pos.findInRange(FIND_STRUCTURES, 3)
				.filter(s => RESOURCE_RETURN_STRUCTURES.includes(s.structureType))
				.sort(compareFn)
				.find(s => (s as StructureWithStore).store.getFreeCapacity(this.resource) || s.hits < s.hitsMax) as StructureWithStore | undefined
			?? self.room.find(FIND_MY_CREEPS)
				.filter(c => c.memory.role === "Mule")
				.sort(compareFn)
				.find(c => c.store.getFreeCapacity(this.resource)) as AnyCreep | undefined;
		if(dropoff)
		{
			const site = dropoff as ConstructionSite;
			const structure = dropoff as StructureWithStore;
			if("progress" in site)
			{
				this.dropoffBehavior = new ConstructBuildingBehavior(site, false) as DropoffBehavior;
			}
			else if("structureType" in structure && structure.hits < structure.hitsMax)
			{
				this.dropoffBehavior = new RepairStructureBehavior(structure, false) as DropoffBehavior;
			}
			else
			{
				this.dropoffBehavior = new SupplyEnergyBehavior(structure) as DropoffBehavior;
			}
			return this.dropoffBehavior.run(self);
		}
		return ERR_INVALID_TARGET;
	}
}

export class MineResourceBehavior extends MoveToAndPerformActionBehavior<ResourceVein>
{
	private returnResourceBehavior = new ReturnResourceBehavior(this.resource);
	private returning = false;
	constructor(vein: ResourceVein, private resource: ResourceConstant)
	{
		super(vein, Creep.prototype.harvest);
	}

	run(self: AnyCreep): BehaviorReturnCode
	{
		const{store} = self;
		const full = store.getFreeCapacity(this.resource) === 0;
		const empty = store.getUsedCapacity(this.resource) === 0;
		if(full){this.returning = true;}
		if(empty){this.returning = false;}
		if(this.returning)
		{
			const result = this.returnResourceBehavior.run(self);
			if(result === ERR_INVALID_TARGET){this.returning = false;}
			else{return result;}
		}
		if(!this.returning)
		{
			return super.run(self);
		}
		throw new Error("Unreachable");
	}

	handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode
	{
		switch(returnCode as CreepActionReturnCode | ERR_NOT_FOUND | ERR_NOT_ENOUGH_RESOURCES)
		{
			case ERR_NOT_OWNER:
			case ERR_NOT_ENOUGH_RESOURCES:
				return ERR_INVALID_TARGET;
			default: throw new Error(`Unhandled return code: ${returnCode}`);
		}
	}
}

export default class MineResourceJob extends JobBase
{
	veinId: Id<ResourceVein>;
	constructor(source: ResourceVein, public resource: ResourceConstant)
	{
		super("MineResource",
			{
				maxAssigned: 3,
				atom: source.id,
				priority: JobPriority.MAINTAIN,
				location: source.pos,
				timeout: 3000,
				behavior: new MineResourceBehavior(source, resource),
			});
		this.veinId = source.id;
	}

	run(): boolean
	{
		const source = Game.getObjectById(this.veinId);
		return MineResourceJob.isJobNeeded(source);
	}

	static isJobNeeded(source: ResourceVein | null): boolean
	{
		if(!source) {return false;}
		const energy = ((source as Source).energy ?? 0) > 0;
		const minerals = ((source as Mineral).mineralAmount ?? 0) > 0;
		const isDeposit = ((source as Deposit).ticksToDecay ?? 0) > 0;
		return energy || minerals || isDeposit;
	}

	toString(): string
	{
		const source = Game.getObjectById(this.veinId);
		return `${super.toString()}:${source?.pos?.roomName || "???"},${source?.pos?.x || "???"},${source?.pos?.y || "???"}`;
	}
}