import JobBase from "./JobBase";
import {getPriorityFromBuildingType} from "organization/BuildingPlanner";
import {BehaviorReturnCode} from "behaviors/BehaviorBase";
import {MoveToAndPerformActionBehavior} from "behaviors/MoveToAndPerformActionBehavior";

export class ConstructBuildingBehavior extends MoveToAndPerformActionBehavior<ConstructionSite>
{
	constructor(target: ConstructionSite, requestResupply = true)
	{
		super(target, Creep.prototype.build);
	}

	handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode
	{
		switch(returnCode as CreepActionReturnCode | ERR_NOT_ENOUGH_ENERGY | ERR_RCL_NOT_ENOUGH)
		{
			case ERR_NOT_OWNER:
			case ERR_NOT_ENOUGH_ENERGY:
			case ERR_RCL_NOT_ENOUGH:
				return ERR_INVALID_TARGET;
			default: throw new Error(`Unhandled return code: ${returnCode}`);
		}
	}
}

export default class ConstructBuildingJob extends JobBase
{
	constructionSiteId: Id<ConstructionSite>;
	constructor(site: ConstructionSite)
	{
		super(
			"ConstructBuilding",
			{
				maxAssigned: 1,
				atom: site.id,
				priority: getPriorityFromBuildingType(site.structureType),
				location: site.pos,
				behavior: new ConstructBuildingBehavior(site),
			}
		);
		this.constructionSiteId = site.id;
	}

	run(): boolean
	{
		const site = Game.getObjectById(this.constructionSiteId);
		return site !== null;
	}
}