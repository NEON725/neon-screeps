import JobBase from "jobs/JobBase";
import CreepMemoryBase from "types/CreepMemoryBase";
import CreepRole from "roles/CreepRole";
import {drawIntent, log, LogLevel, randomDirection} from "utils/misc";
import {BodyBuilder} from "utils/BodyBuilder";

export class WorkerMemory extends CreepMemoryBase
{
}

const HARVEST_JOBS = ["MineResource"];
const SPEND_JOBS = ["UpgradeController", "ConstructBuilding", "RepairStructure"];
const ACCEPTABLE_JOBS = SPEND_JOBS.concat(HARVEST_JOBS);

export default class WorkerRole extends CreepRole
{
	private builder = new BodyBuilder([], [MOVE, CARRY, WORK, WORK, WORK, WORK, WORK]);

	constructor()
	{
		super("Worker");
	}

	initMemory(): CreepMemory
	{
		return new WorkerMemory(this.roleName);
	}

	run(creep: Creep): void
	{
		const memory = creep.memory as WorkerMemory;
		const jobId = memory.assignedJob;
		const job = global.jobQueue.getJobById(jobId);
		if(job)
		{
			const result = job.getDefaultBehavior().run(creep);
			switch(result)
			{
				case ERR_INVALID_TARGET:
					log(LogLevel.WALL, "INVALID", `Unable to complete ${job.toString()}`, creep);
					job.unassignJob(creep);
					break;
				case OK:
				case ERR_BUSY:
					if(job)
					{
						job.resetTimeout();
						drawIntent(creep, job.location);
					}
					break;
				default:
				{
					const jobText = job ? job.toString() : "none";
					throw new Error(`Unknown result: job:${jobText} ${result}`);
				}
			}
		}
		else
		{
			creep.move(randomDirection());
		}
	}

	canAcceptJob(creep: JobAssignable, job: JobBase): boolean
	{
		const worker = creep as AnyCreep;
		const full = worker.store.getFreeCapacity(RESOURCE_ENERGY) === 0;
		return (full ? SPEND_JOBS : ACCEPTABLE_JOBS).includes(job.jobName);
	}

	generateBody(energyBudget: number): BodyPartConstant[]
	{
		return this.builder.generateBody(energyBudget);
	}
}