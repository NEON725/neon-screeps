import JobBase from "jobs/JobBase";
import CreepMemoryBase from "types/CreepMemoryBase";
import CreepRole from "./CreepRole";

export default class StructureTowerRole extends CreepRole
{
	constructor()
	{
		super("StructureTower");
	}

	initMemory(): CreepMemory
	{
		return new CreepMemoryBase(this.roleName);
	}

	run(creep: unknown): void
	{
		const self = creep as StructureTower;
		const enemy = self.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
		if(enemy)
		{
			self.attack(enemy);
		}
		else
		{
			const damaged = self.room.find(FIND_MY_CREEPS).find((c: AnyCreep) => c.hits < c.hitsMax);
			if(damaged) {self.heal(damaged);}
		}
	}

	canAcceptJob(_creep: JobAssignable, _job: JobBase): boolean
	{
		return false;
	}

	generateBody(_cost: number): BodyPartConstant[]
	{
		throw new Error("Method not implemented.");
	}
}