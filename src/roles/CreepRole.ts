import JobBase from "jobs/JobBase";

export default abstract class CreepRole
{
	readonly roleName: string;
	constructor(roleName: string)
	{
		this.roleName = roleName;
	}

	abstract initMemory(): CreepMemory;
	abstract run(creep: JobAssignable): void;
	abstract canAcceptJob(creep: JobAssignable, job: JobBase): boolean;
	abstract generateBody(energyBudget: number): BodyPartConstant[];
}