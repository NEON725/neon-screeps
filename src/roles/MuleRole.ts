import MoveToDestinationBehavior from "behaviors/MoveToDestinationBehavior";
import EmptyStockpileJob from "jobs/EmptyStockpileJob";
import JobBase from "jobs/JobBase";
import SendGiftJob from "jobs/SendGiftJob";
import SupplyEnergyJob from "jobs/SupplyEnergyJob";
import AnyStockpile from "types/AnyStockpile";
import CreepMemoryBase from "types/CreepMemoryBase";
import EnergySupplyDestination, {getEnergySupplyCapacity} from "types/EnergySupplyDestination";
import {BodyBuilder} from "utils/BodyBuilder";
import {drawIntent, log, LogLevel, randomDirection} from "utils/misc";
import CreepRole from "./CreepRole";

class MuleIntent
{
}
export class MuleMemory extends CreepMemoryBase
{
	homeRoom: string | null = null;
	intent: MuleIntent = new MuleIntent();
}

const FILL_JOBS = ["EmptyStockpile"];
const EMPTY_JOBS = ["SendGift", "SupplyEnergy"];

export default class MuleRole extends CreepRole
{
	private builder = new BodyBuilder([], [MOVE, CARRY, CARRY]);

	constructor()
	{
		super("Mule");
	}

	initMemory(): CreepMemory
	{
		return new MuleMemory(this.roleName);
	}

	run(creep: Creep): void
	{
		const job = global.jobQueue.getJobById(creep?.memory.assignedJob) as JobBase | undefined;
		const room = creep.room;
		const memory = creep.memory as MuleMemory;
		if(!memory.homeRoom){memory.homeRoom = room.name;}
		const store = creep.store;
		let targetRoomPosition: RoomPosition | null = null;
		let target: AnyStockpile | null = null;
		if(job)
		{
			if(!this.canAcceptJob(creep, job)){job.unassignJob(creep);}
			else
			{
				switch(job.jobName)
				{
					case "SendGift":
					{
						const giftJob = job as SendGiftJob;
						const targetRoomName = giftJob.roomName;
						const jobRoom = Game.rooms[targetRoomName];
						if(jobRoom)
						{
							const roomTargets = jobRoom.find(FIND_HOSTILE_SPAWNS);
							if(roomTargets.length > 0){target = roomTargets[0];}
							else{job.unassignJob(creep);}
						}
						else
						{
							targetRoomPosition = new RoomPosition(25, 25, targetRoomName);
						}
						break;
					}
					case "SupplyEnergy":
					{
						const chargeJob = job as SupplyEnergyJob;
						const{destId} = chargeJob;
						target = Game.getObjectById(destId);
						break;
					}
					case "EmptyStockpile":
					{
						const emptyJob = job as EmptyStockpileJob;
						const{containerId} = emptyJob;
						target = Game.getObjectById(containerId);
						break;
					}
				}
				if(target)
				{
					const targetOp = (FILL_JOBS.includes(job.jobName)) ?
						(t: AnyStockpile) =>
						{
							const possibleStructure = t as StructureWithStore;
							const possibleResource = t as Resource;
							const possibleRuin = t as Ruin;
							if(possibleRuin.ticksToDecay){return creep.withdraw(possibleRuin, RESOURCE_ENERGY);}
							else if(possibleStructure.structureType){return creep.withdraw(possibleStructure, RESOURCE_ENERGY);}
							else if(possibleResource.amount){return creep.pickup(possibleResource);}
							return ERR_INVALID_TARGET;
						} :
						(t: AnyStockpile) =>
						{
							const dest = t as EnergySupplyDestination;
							const space = getEnergySupplyCapacity(dest);
							const carrying = store.energy;
							if(dest.store){return creep.transfer(dest, RESOURCE_ENERGY, Math.min(space, carrying));}
							return ERR_INVALID_TARGET;
						};
					const result = targetOp(target);
					if(result === ERR_NOT_IN_RANGE || result === ERR_TIRED || result === OK)
					{
						targetRoomPosition = target.pos;
						job.resetTimeout();
					}
					else if(result === ERR_FULL || result === ERR_BUSY)
					{
						drawIntent(creep, target.pos);
					}
					else
					{
						log(LogLevel.WALL, "MULE FAIL", `Got ${result} when performing ${job.jobName} on ${target.id}`, creep);
						job.unassignJob(creep);
						targetRoomPosition = null;
					}
				}
			}
		}
		else
		{
			creep.move(randomDirection());
		}
		if(targetRoomPosition)
		{
			const behavior = new MoveToDestinationBehavior(targetRoomPosition);
			if([OK, ERR_TIRED].includes(behavior.run(creep) as any) && job)
			{
				job.resetTimeout();
			}
		}
	}

	canAcceptJob(creep: JobAssignable, job: JobBase): boolean
	{
		const{jobName} = job;
		const{store} = creep as AnyCreep;
		const empty = store.getUsedCapacity(RESOURCE_ENERGY) === 0;
		const full = store.getFreeCapacity(RESOURCE_ENERGY) === 0;
		const acceptableJobs = (empty ? [] : EMPTY_JOBS).concat(full ? [] : FILL_JOBS);
		let retVal = acceptableJobs.includes(jobName);
		if(jobName === "EmptyStockpile")
		{
			retVal = retVal && (job as EmptyStockpileJob).resource === RESOURCE_ENERGY;
		}
		return retVal;
	}

	generateBody(energyBudget: number): BodyPartConstant[]
	{
		return this.builder.generateBody(energyBudget);
	}
}