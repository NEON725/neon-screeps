import {ErrorMapper} from "utils/ErrorMapper";
import RoleIndex from "roles/RoleIndex";
import JobQueue from "jobs/JobQueue";
import CreepRosterMeta from "types/CreepRosterMeta";
import JobBase from "jobs/JobBase";
import {compareAscending, generateCompareDistanceFrom, getAllJobAssignable, log, LogLevel, setLogLevel} from "utils/misc";
import UpgradeControllerJob from "jobs/UpgradeControllerJob";
import ConstructBuildingJob from "jobs/ConstructBuildingJob";
import watcher from "screeps-multimeter/lib/watch-client";
import MapIntel, {RoomAllocation, RoomIntel} from "organization/MapIntel";
import BuildingPlanner from "organization/BuildingPlanner";
import Profiler from "screeps-profiler/screeps-profiler";
import EmptyStockpileJob from "jobs/EmptyStockpileJob";
import MineResourceJob, {RESOURCE_RETURN_STRUCTURES} from "jobs/MineResourceJob";
import SupplyEnergyJob from "jobs/SupplyEnergyJob";
import AnyStockpile from "types/AnyStockpile";
import RepairStructureJob from "jobs/RepairStructureJob";

type BasicVoidFuncType = ()=> void;
declare global
{
	interface JobAssignableMemory
	{
		role: string;
		assignedJob: Id<JobBase> | null | undefined;
	}
	interface CreepMemory extends JobAssignableMemory{}
	interface PowerCreepMemory extends CreepMemory{}
	interface PowerCreep
	{
		room: Room;
	}
	interface SpawnMemory extends JobAssignableMemory{}
	interface Structure
	{
		my: boolean | undefined;
	}
	interface JobAssignable extends _HasRoomPosition, _HasId
	{
		name: string;
		memory: JobAssignableMemory;
	}
	interface StructureWithStore extends Structure
	{
		store: Store<RESOURCE_ENERGY, false>;
	}
	interface StructureSpawn extends JobAssignable {}
	interface RoomMemory extends RoomIntel {}
	namespace NodeJS
	{
		interface Global
		{
			killAllCreeps: BasicVoidFuncType;
			jobQueue: JobQueue;
			fillableJobs: JobBase[];
			roleIndex: RoleIndex;
			creepRosterMeta: CreepRosterMeta;
			runOnRole: any;
			pingRole: any;
			setLogLevel: any;
			getStatusLine: any;
			forgetAllRooms: any;
			removeAllConstructionSites: any;
			getCreepJob: any;
		}
	}
}

global.setLogLevel = setLogLevel;

global.killAllCreeps = function()
{
	for(const name in Game.creeps)
	{
		Game.creeps[name].suicide();
		delete Game.creeps[name];
	}
};

global.runOnRole = function(roleName: string, callback: (c: Creep)=> void)
{
	for(const name in Game.creeps)
	{
		const creep = Game.creeps[name];
		if(creep.memory.role === roleName){callback(creep);}
	}
};

global.pingRole = function(roleName: string)
{
	global.runOnRole(roleName, (c: Creep)=>c.say("Here!"));
};

global.forgetAllRooms = function()
{
	for(const roomName in Memory.rooms){delete Memory.rooms[roomName];}
	Memory.rooms = {};
};

global.removeAllConstructionSites = function()
{
	for(const siteId in Game.constructionSites)
	{
		const site = Game.constructionSites[siteId];
		site.remove();
	}
};

global.getCreepJob = function(creepName: string)
{
	const jobId = Game.creeps[creepName].memory.assignedJob ?? "none";
	return global.jobQueue.jobs.find(job => job.id === jobId);
};

for(const name in Game.creeps)
{
	const creep = Game.creeps[name];
	const memory = creep?.memory;
	if(memory?.assignedJob)
	{
		memory.assignedJob = null;
	}
}

const jobQueue = new JobQueue();
jobQueue.flushJobs();
global.jobQueue = jobQueue;
const roleIndex = new RoleIndex();
global.roleIndex = roleIndex;
global.getStatusLine = function(): string
{
	/* eslint-disable @typescript-eslint/restrict-template-expressions */
	/* eslint-disable @typescript-eslint/restrict-plus-operands */
	const roleText = global.roleIndex.roleNames.
		filter((roleName: string) => !roleName.startsWith("Structure") && roleName !== "Scout").
		map((roleName: string)=>`${roleName}:${global.creepRosterMeta?.getIdle(roleName)}/${global.creepRosterMeta?.getTotal(roleName)}`).
		join(" ");
	const popText = `POP:x${global.creepRosterMeta?.total} ${roleText}`;
	const jobQueueUnassignedTallies: any = {};
	const jobQueueMaxTallies: any = {};
	global.jobQueue.jobs.forEach((job)=>
	{
		jobQueueUnassignedTallies[job.jobName] = (jobQueueUnassignedTallies[job.jobName] || 0) + job.maxAssigned - job.assigned.length;
		jobQueueMaxTallies[job.jobName] = (jobQueueMaxTallies[job.jobName] || 0) + job.maxAssigned;
	});
	const jobQueueText = `x${global.jobQueue.jobs.length} ${Object.keys(jobQueueMaxTallies).map((jobName: string)=>`${jobName}:${jobQueueUnassignedTallies[jobName]}/${jobQueueMaxTallies[jobName]}`).join(" ")}`;
	return `${popText} JOBS:${jobQueueText}`;
	/* eslint-enable */
};

Profiler.enable();
log(LogLevel.EVENT, "SYSTEM", "INIT COMPLETE");

export const loop = ErrorMapper.wrapLoop(() =>
{
	const cpu = Game.cpu;
	log(LogLevel.WALL, "TICK", `TICK: ${Game.time} CPU:${cpu.bucket}`);
	if(cpu.bucket < cpu.tickLimit)
	{
		Profiler.output();
		log(LogLevel.DANGER, "CPU", `CPU throttling: ${cpu.bucket}/${cpu.tickLimit}(${cpu.limit})`);
		return;
	}
	else if(cpu.bucket >= 10000 && cpu.generatePixel && cpu.generatePixel() === OK){log(LogLevel.INFO, "CPU", "Generated pixel.");}
	Profiler.wrap(()=>
	{
		for(const name in Memory.creeps)
		{
			if(!(name in Game.creeps))
			{
				const memory = Memory.creeps[name];
				log(LogLevel.EVENT, "DEAD", "Creep died...", {name, memory} as JobAssignable);
				const job = jobQueue.getJobById(memory.assignedJob);
				if(job){job.unassignJob(name);}
				delete Memory.creeps[name];
			}
		}

		const allAssignable = getAllJobAssignable();

		const compareDistanceFromMainRoomPos = generateCompareDistanceFrom(Game.spawns.Spawn1.pos);
		global.fillableJobs = jobQueue.run()
			.sort((a, b)=>
			{
				const retVal = compareAscending(a.priority, b.priority);
				return retVal ? (retVal * -1) : compareDistanceFromMainRoomPos(a.location, b.location);
			})
			.filter(job =>
			{
				const compareFn = generateCompareDistanceFrom(job.location);
				allAssignable.
					sort(compareFn).
					forEach(creep => jobQueue.attemptFillJob(creep, job));
				return job.capacity();
			});

		const creepRosterMeta = new CreepRosterMeta();
		global.creepRosterMeta = creepRosterMeta;
		for(const ent of allAssignable)
		{
			try
			{
				const memory = ent.memory;
				const role = roleIndex.getRole(memory.role || ent);
				role.run(ent);
				if("body" in ent){creepRosterMeta.tallyCreep(ent as AnyCreep);}
			}
			catch(e)
			{
				log(LogLevel.DANGER, "SCRIPT ERROR", `Script error for ${JSON.stringify(ent.memory)}: ${(e as Error).message}`, ent);
				const{stack} = e as Error;
				if(stack){log(LogLevel.DANGER, "SCRIPT ERROR", stack, ent);}
			}
		}

		for(const structure of Object.values(Game.structures))
		{
			switch(structure.structureType)
			{
				case STRUCTURE_TOWER:
				{
					const tower = structure as StructureTower;
					const role = roleIndex.getRole("StructureTower");
					role.run(tower as any);
				}
				/* eslint-disable-next-line no-fallthrough */
				case STRUCTURE_SPAWN:
				case STRUCTURE_EXTENSION:
				{
					const spawn = structure as StructureWithStore;
					if(spawn.my && SupplyEnergyJob.isJobNeeded(spawn))
					{
						const fillJob = new SupplyEnergyJob(spawn);
						jobQueue.addJob(fillJob);
					}
					break;
				}
				case STRUCTURE_CONTROLLER:
				{
					const controller = structure as StructureController;
					if(controller.my && UpgradeControllerJob.isJobNeeded(controller))
					{
						const upgradeJob = new UpgradeControllerJob(controller);
						jobQueue.addJob(upgradeJob);
					}
					break;
				}
				default:
					break;
			}
		}

		for(const name in Game.constructionSites)
		{
			const site = Game.constructionSites[name];
			const constructJob = new ConstructBuildingJob(site);
			jobQueue.addJob(constructJob);
		}

		MapIntel.run();
		BuildingPlanner.run();

		for(const roomName in Memory.rooms)
		{
			const memory = Memory.rooms[roomName];
			const{allocation} = memory;
			const room = Game.rooms[roomName];
			if(!room){continue;}
			const{visual} = room;

			if([RoomAllocation.INDUSTRIAL, RoomAllocation.MILITARY].includes(allocation ?? RoomAllocation.NEUTRAL))
			{
				const spawns = room.find(FIND_MY_SPAWNS);
				if(spawns.length > 0)
				{
					const spawnJobs = creepRosterMeta.generateSpawnJobs(room);
					for(const job of spawnJobs)
					{
						jobQueue.addJob(job);
					}
				}

				const sources = room.find(FIND_SOURCES_ACTIVE);
				for(const source of sources)
				{
					const mineJob = new MineResourceJob(source, RESOURCE_ENERGY);
					jobQueue.addJob(mineJob);
				}

				const structures = room.find(FIND_STRUCTURES);

				const stockpiles = structures
					.filter(s=>RESOURCE_RETURN_STRUCTURES.includes((s as Structure).structureType))
					.map(s=>s as AnyStockpile)
					.concat(room.find(FIND_RUINS) as AnyStockpile[]);
				for(const stockpile of stockpiles)
				{
					if(EmptyStockpileJob.isJobNeeded(stockpile, RESOURCE_ENERGY))
					{
						const emptyJob = new EmptyStockpileJob(stockpile, RESOURCE_ENERGY);
						jobQueue.addJob(emptyJob);
					}
				}
				const resources = room.find(FIND_DROPPED_RESOURCES);
				for(const resource of resources)
				{
					const emptyJob = new EmptyStockpileJob(resource, RESOURCE_ENERGY);
					jobQueue.addJob(emptyJob);
				}

				const damagedStructures = structures.filter(s=>s.hits < s.hitsMax);
				for(const damaged of damagedStructures)
				{
					const repairJob = new RepairStructureJob(damaged);
					jobQueue.addJob(repairJob);
				}
			}

			if(allocation !== RoomAllocation.NEUTRAL)
			{
				const allocationText = RoomAllocation[allocation || RoomAllocation.NEUTRAL];
				const mapVisual = Game.map.visual;
				mapVisual.text(allocationText, new RoomPosition(25, 5, roomName), {fontSize: 4});
				visual.text(allocationText, 25, 0);
				const color = (memory.averageEnergySourceUtilizationRatio === 1) ? "#00dd00" : "#ffffff";
				visual.text(`Energy Utilization: ${Math.floor(memory.averageEnergySourceUtilizationRatio * 100)}%`, 25, 1, {color});
			}
		}

		watcher();
	});
});