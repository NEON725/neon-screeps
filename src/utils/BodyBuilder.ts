export class BodyBuilder
{
	constructor(private initial: BodyPartConstant[], private repeating: BodyPartConstant[])
	{
	}

	generateBody(energyBudget: number): BodyPartConstant[]
	{
		let budget = energyBudget;
		const retVal = [...this.initial];
		for(const part of retVal) {budget -= BODYPART_COST[part];}
		if(budget < 0){return [];}
		while(true)
		{
			for(const part of this.repeating)
			{
				const cost = BODYPART_COST[part];
				if(budget < cost){return retVal;}
				budget -= cost;
				retVal.push(part);
			}
		}
	}
}