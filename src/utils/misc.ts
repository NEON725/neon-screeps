import JobPriority from "jobs/JobPriority";
import names from "utils/names.json";

export function deepEquals(a: Record<string, unknown>, b: Record<string, unknown>): boolean
{
	if(Object.keys(a).length !== Object.keys(b).length)
	{
		return false;
	}

	const aKeys = Object.keys(a);
	const bKeys = Object.keys(b);

	if(aKeys.find(key=>bKeys.indexOf(key) === -1) || bKeys.find(key=>aKeys.indexOf(key) === -1))
	{
		return false;
	}

	for(const key of aKeys)
	{
		const aValue = a[key];
		const bValue = b[key];
		if(typeof(aValue) !== typeof(bValue))
		{
			return false;
		}
		if(typeof(aValue) === "object")
		{
			if(!deepEquals(aValue as Record<string, unknown>, bValue as Record<string, unknown>))
			{
				return false;
			}
		}
		else if(aValue !== bValue)
		{
			return false;
		}
	}
	return true;
}

export function randomDirection(): DirectionConstant
{
	return (Math.floor(Math.random() * 8) + 1) as DirectionConstant;
}

export function generateID<T extends _HasId>(): Id<T>
{
	return ("xxxx-xxxx-xxx-xxxx".replace(/[x]/g, function(c)
	{
		/* eslint-disable no-bitwise */
		const r = Math.random() * 16 | 0; const v = c === "x" ? r : (r & 0x3 | 0x8);
		return v.toString(16);
		/* eslint-enable no-bitwise */
	})) as Id<T>;
}

export function generateRandomName(): string
{
	while(true)
	{
		const i = Math.floor(Math.random() * names.length);
		const retval = names[i];
		if(!Game.creeps[retval]){return retval;}
	}
}

export function isJobAssignable(ent: RoomObject): boolean
{
	const a = ent as any;
	return ("name" in a) && ("memory" in a);
}

export function getAllJobAssignable(): JobAssignable[]
{
	return (Object.values(Game.creeps) as any).
		concat(Object.values(Game.structures) as any).
		concat(Object.values(Game.spawns) as any).
		filter(isJobAssignable) as JobAssignable[];
}

export function getJobAssignableByName(name: string): JobAssignable | undefined
{
	return Game.creeps[name] ?? Game.structures[name] ?? Game.spawns[name] ?? undefined;
}

export function tryCastAsJobAssignable(ent: unknown): JobAssignable | undefined
{
	return (typeof(ent) === "object" && isJobAssignable(ent as RoomObject)) ? (ent as JobAssignable) : undefined;
}

export function padString(base: string, length: number, pad?: string, left?: boolean): string
{
	const _pad = pad || " ";
	const padLength = length - base.length;
	const repetitions = Math.ceil(padLength / _pad.length);
	let padding = "";
	for(let i = 0; i < repetitions; i++){padding += _pad;}
	padding = padding.substr(0, padLength);
	return left ? padding + base : base + padding;
}

export enum LogLevel
{ /* eslint-disable-line @typescript-eslint/indent */
	WALL,
	INFO,
	EVENT,
	DANGER,
}

let logLevel: LogLevel = LogLevel.INFO;
export function setLogLevel(level: LogLevel): void {logLevel = level;}

const PREFIX_LENGTH = 12;
const NAME_LENGTH = 12;
const ROLE_LENGTH = 8;
const NAME_COLOR = "#00c8ff";
const ROLE_COLOR = "#55cc55";
const DEFAULT_COLOR = "#ffffff";
const DULL_COLOR = "#999999";
const LEVEL_COLORS: string[] = [];
LEVEL_COLORS[LogLevel.WALL] = DULL_COLOR;
LEVEL_COLORS[LogLevel.EVENT] = "#aaaaff";
LEVEL_COLORS[LogLevel.DANGER] = "#ff4444";

export function log(level: LogLevel, prefix: string, message: string, creep?: JobAssignable): void
{
	if(level >= logLevel)
	{
		const ignoreColumnColors = level <= LogLevel.WALL;
		const prefixColorCode = `${LEVEL_COLORS[level] || DEFAULT_COLOR}-fg`;
		const prefixPad = padString(prefix, PREFIX_LENGTH);
		let nameLogged; let roleLogged;
		const nameColor = ignoreColumnColors ? LEVEL_COLORS[level] : NAME_COLOR;
		const roleColor = ignoreColumnColors ? LEVEL_COLORS[level] : ROLE_COLOR;
		if(creep)
		{
			nameLogged = `{${nameColor}-fg}${padString(creep.name, NAME_LENGTH)}{/${nameColor}-fg}`;
			const memory = creep.memory;
			const roleName = memory?.role;
			if(roleName){roleLogged = `{${roleColor}-fg}${padString(roleName, ROLE_LENGTH)}{/${roleColor}-fg}`;}
		}
		if(!nameLogged){nameLogged = `{${DULL_COLOR}-fg}${padString("", NAME_LENGTH, "_")}{/${DULL_COLOR}-fg}`;}
		if(!roleLogged){roleLogged = `{${DULL_COLOR}-fg}${padString("", ROLE_LENGTH, "_")}{/${DULL_COLOR}-fg}`;}
		console.log(`{${prefixColorCode}}${prefixPad}{/${prefixColorCode}}|${nameLogged}|${roleLogged}| {${prefixColorCode}}${message}{/${prefixColorCode}}`);
	}
}

export function tempDebug(msg: string, creep?: JobAssignable): void
{
	setLogLevel(LogLevel.WALL);
	log(LogLevel.WALL, "DEBUG", msg, creep);
}

export function isExit(pos: RoomPosition): boolean
{
	return pos.x === 0 || pos.x === 49 || pos.y === 0 || pos.y === 49;
}

export function* generateSpiral(origin: string | RoomPosition, maxSpots = 2500): Generator<RoomPosition, null>
{
	const centerPos = (typeof(origin) === "string") ? new RoomPosition(24, 24, origin) : (origin);
	// I have no idea how this math works but the output seems correct so I turned it in to a generator function.
	let x = 0; let y = 0; let dx = 0; let dy = -1;
	for(let i = 0; i < maxSpots; i++)
	{
		try{yield new RoomPosition(x + centerPos.x, y + centerPos.y, centerPos.roomName);}
		catch(e: any){return null;}
		if(x === y || (x < 0 && x === -y) || (x > 0 && x === 1 - y))
		{
			const swap = -dy;
			dy = dx;
			dx = swap;
		}
		x += dx;
		y += dy;
	}
	return null;
}

export function drawIntent(creep: Creep, pos: RoomPosition): void
{
	const{visual, name} = creep.room;
	if(name === pos.roomName){visual.line(creep.pos.x, creep.pos.y, pos.x, pos.y, {width: 0.025, color: "#ff8", opacity: 0.125});}
}

export function compareAscending(a: number, b: number): number
{
	if(a === b) {return 0;}
	return a < b ? -1 : 1;
}

export function generateCompareDistanceFrom(origin: _HasRoomPosition | RoomPosition): (a: _HasRoomPosition | RoomPosition, b: _HasRoomPosition | RoomPosition)=> number
{
	const oPos = (origin as _HasRoomPosition).pos ?? origin;
	return (a: _HasRoomPosition | RoomPosition, b: _HasRoomPosition | RoomPosition) =>
	{
		const aPos = (a as _HasRoomPosition).pos ?? a;
		const bPos = (b as _HasRoomPosition).pos ?? b;
		let aDist;
		let bDist;
		if(aPos.roomName !== oPos.roomName || aPos.roomName !== oPos.roomName)
		{
			aDist = Game.map.getRoomLinearDistance(oPos.roomName, aPos.roomName) * 50;
			bDist = Game.map.getRoomLinearDistance(oPos.roomName, bPos.roomName) * 50;
		}
		else
		{
			aDist = aPos.getRangeTo(oPos.x, oPos.y);
			bDist = bPos.getRangeTo(oPos.x, oPos.y);
		}
		return compareAscending(aDist, bDist);
	};
}

export function logStackTrace(): void
{
	try{throw new Error("Print stack trace:");}
	catch(e)
	{
		const stack = (e as Error).stack ?? "unknown";
		tempDebug(stack);
	}
}

export function getLogLevelFromJobPriority(prio: JobPriority): LogLevel
{
	switch(prio)
	{
		case JobPriority.DANGER: return LogLevel.DANGER;
		case JobPriority.MAINTAIN: return LogLevel.INFO;
		default: return LogLevel.WALL;
	}
}