import JobBase, {JobBaseConfigurator} from "jobs/JobBase";
import JobPriority from "jobs/JobPriority";
import SupplyEnergyJob from "jobs/SupplyEnergyJob";
import {log, LogLevel} from "utils/misc";
import BehaviorBase, {BehaviorReturnCode} from "./BehaviorBase";

export default class RequestSupplyBehavior extends BehaviorBase
{
	run(self: AnyCreep, job?: JobBase, extraOpts?: Partial<JobBaseConfigurator>): BehaviorReturnCode
	{
		log(LogLevel.WALL, "NEED ENERGY", `Ran out of energy during ${job?.jobName || "unassigned"}.`, self);
		const supplyJob = new SupplyEnergyJob(self,
			{
				maxAssigned: 1,
				timeout: 1,
				priority: job?.priority ?? JobPriority.TIMESINK,
				...extraOpts,
			});
		global.jobQueue.addJob(supplyJob);
		return OK;
	}
}