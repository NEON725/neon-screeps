import NoBodyPartException from "types/NoBodyPartException";
import {BehaviorReturnCode} from "./BehaviorBase";
import MoveToDestinationBehavior from "./MoveToDestinationBehavior";

type PerformActionTarget = _HasRoomPosition & _HasId;

export abstract class MoveToAndPerformActionBehavior<TargetType extends PerformActionTarget> extends MoveToDestinationBehavior
{
	targetId: Id<_HasId>;
	constructor(target: TargetType, private action: (target: TargetType)=> ScreepsReturnCode)
	{
		super(target);
		this.targetId = target.id;
	}

	run(self: AnyCreep): BehaviorReturnCode
	{
		const target = Game.getObjectById(this.targetId) as TargetType;
		if(target)
		{
			const retVal = this.action.call(self, target);
			switch(retVal)
			{
				case OK:
				case ERR_TIRED:
				case ERR_BUSY:
				case ERR_NOT_IN_RANGE:
				case ERR_NOT_FOUND:
				case ERR_NO_PATH:
					break;
				case ERR_NO_BODYPART: throw new NoBodyPartException(self);
				default: return this.handleReturnCode(self, retVal);
			}
		}
		return super.run(self);
	}

	abstract handleReturnCode(self: AnyCreep, returnCode: ScreepsReturnCode): BehaviorReturnCode;
}