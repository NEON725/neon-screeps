import BehaviorBase, {BehaviorReturnCode} from "./BehaviorBase";

export default class MoveToDestinationBehavior extends BehaviorBase
{
	private target: RoomPosition;
	private extraOpts: Record<string, unknown>;
	constructor(
		target: RoomPosition | _HasRoomPosition,
		extraOpts?: Record<string, unknown>
	)
	{
		super();
		this.target = ("pos" in target) ? target.pos : target;
		this.extraOpts = {reusePath: 20, ...(extraOpts || {visualizePathStyle: {}})};
	}

	run(self: AnyCreep): BehaviorReturnCode
	{
		let retVal = self.moveTo(this.target, this.extraOpts);
		if(retVal === ERR_INVALID_TARGET)
		{
			this.target = new RoomPosition(this.target.x, this.target.y, this.target.roomName);
			retVal = self.moveTo(this.target, this.extraOpts);
		}
		switch(retVal)
		{
			case ERR_NOT_FOUND:
			case ERR_NOT_OWNER:
				return ERR_INVALID_TARGET;
			case ERR_TIRED: return ERR_BUSY;
			case ERR_NO_BODYPART: throw new Error(`Tried to perform MoveToDestinationBehavior with invalid creep: ${self.name}`);
			default: return retVal;
		}
	}
}