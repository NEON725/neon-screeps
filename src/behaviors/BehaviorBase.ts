export type BehaviorReturnCode = OK | ERR_INVALID_TARGET | ERR_NO_PATH | ERR_BUSY;

export default abstract class BehaviorBase
{
	abstract run(self: AnyCreep): BehaviorReturnCode;
}

export class NotImplementedBehavior
{
	run(_self: AnyCreep): BehaviorReturnCode
	{
		throw new Error("Not implemented.");
	}
}