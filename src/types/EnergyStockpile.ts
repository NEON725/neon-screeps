import EnergySupplyDestination, {getEnergySupplyAmount} from "./EnergySupplyDestination";

type EnergyStockpile = EnergySupplyDestination | Resource | Ruin;

export default EnergyStockpile;

export const getStockpiledEnergyAmount = (stockpile: EnergyStockpile): number =>
{
	const possibleResource = stockpile as Resource;
	if("amount" in possibleResource){return possibleResource.amount;}
	else{return getEnergySupplyAmount(stockpile as EnergySupplyDestination);}
};