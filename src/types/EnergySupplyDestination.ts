type EnergySupplyDestination = StructureWithStore | AnyCreep;

export default EnergySupplyDestination;

export const getEnergySupplyAmount = (stockpile: EnergySupplyDestination): number =>
{
	return stockpile.store.getUsedCapacity(RESOURCE_ENERGY) ?? 0;
};
export const getEnergySupplyCapacity = (stockpile: EnergySupplyDestination): number =>
{
	return stockpile.store.getFreeCapacity(RESOURCE_ENERGY) ?? 0;
};