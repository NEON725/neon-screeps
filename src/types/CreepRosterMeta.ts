import JobBase from "jobs/JobBase";
import JobPriority from "jobs/JobPriority";
import SpawnCreepJob from "jobs/SpawnCreepJob";
import {compareAscending} from "utils/misc";

const ECONOMY_ROLES = ["Worker", "Mule"];
const MINIMUM_ECONOMY_ROLE_COUNT = 6;
const MINIMUM_BUDGET = 300;
const POPULATION_MAX = 250;
const ROLE_TARGETS_BY_RCL =
[
	{
	},
	{
		"Scout": 10,
	},
] as Record<string, number>[];

export default class CreepRosterMeta
{
	total = 0;
	idleByRole: {[key: string]: number} = {};
	totalsByRole: {[key: string]: number} = {};
	totalsByRoom: {[key: string]: number} = {};
	private positionSumByRoom: {[key: string]: {x: number, y: number}} = {};

	tallyCreep(creep: AnyCreep): void
	{
		this.total++;
		this.totalsByRole[creep.memory.role] = (this.totalsByRole[creep.memory.role] || 0) + 1;
		if(!creep.memory.assignedJob){this.idleByRole[creep.memory.role] = (this.idleByRole[creep.memory.role] || 0) + 1;}
		const roomName = creep.room.name;
		const positionSum = this.positionSumByRoom[roomName] || {x: 0, y: 0};
		positionSum.x += creep.pos.x;
		positionSum.y += creep.pos.y;
		this.positionSumByRoom[roomName] = positionSum;
		this.positionSumByRoom[roomName] = positionSum;
		this.totalsByRoom[roomName] = (this.totalsByRoom[roomName] || 0) + 1;
	}

	getTotal(role: string): number
	{
		return this.totalsByRole[role] || 0;
	}

	getIdle(role: string): number
	{
		return this.idleByRole[role] || 0;
	}

	compareRolesByLowestCount(roleA: string, roleB: string): number
	{
		const aCount = this.totalsByRole[roleA];
		const bCount = this.totalsByRole[roleB];
		return compareAscending(aCount, bCount);
	}

	generateSpawnJobs(room: Room): JobBase[]
	{
		if(this.total >= POPULATION_MAX){return [];}
		const budget = room.energyCapacityAvailable;
		const pos = room.getPositionAt(25, 25) as RoomPosition;
		const minimums = ECONOMY_ROLES
			.filter(role => (this.totalsByRole[role] || 0) < MINIMUM_ECONOMY_ROLE_COUNT)
			.sort(this.compareRolesByLowestCount.bind(this))
			.map(role => new SpawnCreepJob(role, `minimum-${role}`, JobPriority.DANGER, pos, MINIMUM_BUDGET));
		const rcl = room.controller?.level ?? 0;
		const targets = global.roleIndex.roleNames
			.map(role =>
			{
				let targetCount = 0;
				for(let i = rcl - 1; i >= 0; i--)
				{
					const targetsInRcl = ROLE_TARGETS_BY_RCL[i];
					if(targetsInRcl && role in targetsInRcl)
					{
						targetCount = targetsInRcl[role];
						break;
					}
				}
				return {role, ratio: this.totalsByRole[role] / targetCount};
			})
			.filter(({ratio}) => ratio < 1)
			.sort(({ratio: ratioA}, {ratio: ratioB}) => compareAscending(ratioA, ratioB))
			.map(({role}) => new SpawnCreepJob(role, `target-${role}`, JobPriority.MAINTAIN, pos, budget));
		const needed = ECONOMY_ROLES
			.filter(role => !this.getIdle(role))
			.sort((roleA, roleB) => compareAscending(this.getTotal(roleA), this.getTotal(roleB)))
			.map(role => new SpawnCreepJob(role, `needed-${role}`, JobPriority.EXPAND, pos, budget));
		const retVal = minimums.concat(targets).concat(needed);
		if(!retVal.length){retVal.push(new SpawnCreepJob("Scout", "idle-spawn", JobPriority.TIMESINK, pos, budget));}
		return retVal;
	}

	getPositionAverage(room: Room | string): RoomPosition
	{
		const roomName = (typeof(room) === "string") ? (room as unknown as string) : (room as unknown as Room).name;
		const positionSum = this.positionSumByRoom[roomName] || new RoomPosition(24, 24, roomName);
		const roomTotal = this.totalsByRoom[roomName] || 0;
		return new RoomPosition(Math.floor(positionSum.x / roomTotal), Math.floor(positionSum.y / roomTotal), roomName);
	}
}