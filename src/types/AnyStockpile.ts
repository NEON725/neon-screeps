import EnergyStockpile, {getStockpiledEnergyAmount} from "./EnergyStockpile";

type AnyStockpile = EnergyStockpile | StructureWithStore | Ruin;
export default AnyStockpile;

export const getStockpiledResourceAmount = (stockpile: AnyStockpile, res: ResourceConstant): number =>
{
	const typeless = stockpile as any;
	if("store" in stockpile){return (typeless.store.getUsedCapacity(res) ?? 0) as number;}
	else if(res === RESOURCE_ENERGY){return getStockpiledEnergyAmount(stockpile as EnergyStockpile);}
	else{throw new Error(`getStockpiledAmount: Unknown resource ${res}: ${Object.keys(typeless).join(",")}`);}
};