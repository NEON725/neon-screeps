export default class NoBodyPartException extends Error
{
	constructor(creep: AnyCreep)
	{
		const partsList = ((creep as Creep).body ?? []).join(",");
		super(`Tried to perform behavior with invalid creep: ${creep.name} [${partsList}]`);
	}
}