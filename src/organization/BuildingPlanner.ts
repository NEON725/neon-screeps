import {generateSpiral, log, LogLevel} from "utils/misc";
import {RoomAllocation} from "./MapIntel";
import JobPriority from "jobs/JobPriority";

const MAX_PLACEMENT_ATTEMPTS = 2500;
const MAX_SIGNIFICANT_CONSTRUCTION_SITES = 3;
const MAX_INSIGNIFICANT_CONSTRUCTION_SITES = 1;
const INSIGNIFICANT_BUILDING_TYPES: StructureConstant[] = [STRUCTURE_ROAD, STRUCTURE_WALL];
const EMERGENCY_BUILDING_TYPES: StructureConstant[] = [STRUCTURE_TOWER];

export const BUILD_TARGETS: BuildableStructureConstant[][] =
[
	[STRUCTURE_SPAWN],
	[
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
	],
	[
		STRUCTURE_TOWER,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
	],
	[
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
		STRUCTURE_EXTENSION,
	],
];

enum BuildPlacementType
{ /* eslint-disable-line @typescript-eslint/indent */
	RANDOM,
	GRID,
	GRID_ROAD,
	POPULATION_AVERAGE,
}

export const getPriorityFromBuildingType = (type: StructureConstant): JobPriority =>
{
	if(EMERGENCY_BUILDING_TYPES.includes(type)){return JobPriority.DANGER;}
	return (INSIGNIFICANT_BUILDING_TYPES.includes(type)) ? JobPriority.TIMESINK : JobPriority.EXPAND;
};

export default class BuildingPlanner
{
	static run(): void
	{
		for(const roomName in Game.rooms)
		{
			const mapIntel = Memory.rooms[roomName];
			if(!mapIntel){continue;}
			const room = Game.rooms[roomName];
			const sites = room.find(FIND_MY_CONSTRUCTION_SITES);
			switch(mapIntel.allocation)
			{
				case RoomAllocation.INDUSTRIAL:
					BuildingPlanner.planIndustrialDistrict(room, sites);
					break;
				case RoomAllocation.MILITARY:
					BuildingPlanner.planEnergySourceStockpile(room);
					break;
			}
		}
	}

	static planIndustrialDistrict(room: Room, sites: ConstructionSite[]): void
	{
		BuildingPlanner.planEnergySourceStockpile(room);
		const controller = room?.controller;
		if(!controller || !controller.my){return;}
		const rcl = controller.level;
		const significantSites = sites.filter(site => !INSIGNIFICANT_BUILDING_TYPES.includes(site.structureType));
		const insignificantSites = sites.filter(site => INSIGNIFICANT_BUILDING_TYPES.includes(site.structureType));
		const currentBuildTargets = BUILD_TARGETS
			.slice(0, rcl)
			.reduce(
				(prev: BuildableStructureConstant[], next: BuildableStructureConstant[])=>
					prev.concat(next),
				[] as BuildableStructureConstant[]
			);
		const myBuiltStructureTallies: {[key: string]: number} = {};
		const tallyStructureFunc = (structure: {structureType: string})=>
		{
			myBuiltStructureTallies[structure.structureType]
				= (myBuiltStructureTallies[structure.structureType] || 0) + 1;
		};
		room.find(FIND_MY_STRUCTURES).forEach(tallyStructureFunc);
		significantSites.forEach(tallyStructureFunc);
		const buildTargetTalliesCumulative: {[key: string]: number} = {};
		if(significantSites.length < MAX_SIGNIFICANT_CONSTRUCTION_SITES)
		{
			let placedSite = false;
			currentBuildTargets.forEach((structureType: BuildableStructureConstant)=>
			{
				if(placedSite){return;}
				const buildCountTarget = (buildTargetTalliesCumulative[structureType] || 0) + 1;
				buildTargetTalliesCumulative[structureType] = buildCountTarget;
				if((myBuiltStructureTallies[structureType] || 0) < buildCountTarget)
				{
					let attempts = 0;
					for(const location of BuildingPlanner.findConstructionLocation(room.name, BuildingPlanner.getPlacementTypeByStructure(structureType)))
					{
						const result = room.createConstructionSite(location, structureType);
						if(result === OK)
						{
							log(LogLevel.INFO, "CONSTRUCT", `Planned new ${structureType} at ${location.x},${location.y}.`);
							placedSite = true;
							break;
						}
						else if(++attempts >= MAX_PLACEMENT_ATTEMPTS){break;}
					}
				}
			});
			if(!placedSite && insignificantSites.length < MAX_INSIGNIFICANT_CONSTRUCTION_SITES)
			{
				let attempts = 0;
				for(const location of BuildingPlanner.findConstructionLocation(room.name, BuildPlacementType.GRID_ROAD))
				{
					if(room.createConstructionSite(location, STRUCTURE_ROAD) === OK)
					{
						log(LogLevel.WALL, "CONSTRUCT", `Planned new road at ${room.name}:${location.x},${location.y}`);
						break;
					}
					if(++attempts >= MAX_PLACEMENT_ATTEMPTS){break;}
				}
			}
		}
	}

	static planEnergySourceStockpile(room: Room): boolean
	{
		const terrain = room.getTerrain();
		let retVal = false;
		room.find(FIND_SOURCES).forEach((source: Source)=>
		{
			if(retVal){return;}
			let foundExistingContainer = false;
			let containerPlacement: RoomPosition | null = null;
			for(const location of generateSpiral(source.pos, 9))
			{
				if(location.x === source.pos.x && location.y === source.pos.y){continue;}
				const terrainType = terrain.get(location.x, location.y);
				if(terrainType !== TERRAIN_MASK_WALL)
				{
					if(!containerPlacement){containerPlacement = location;}
					const objs = room.lookAt(location.x, location.y);
					for(const obj of objs)
					{
						const{structureType} = obj.structure ?? obj.constructionSite ?? {};
						if(structureType === STRUCTURE_CONTAINER){foundExistingContainer = true;}
					}
				}
			}
			if(!foundExistingContainer && containerPlacement !== null)
			{
				retVal = room.createConstructionSite(containerPlacement, STRUCTURE_CONTAINER) === OK;
			}
		});
		return retVal;
	}

	static findConstructionLocation = function*(roomName: string, type: BuildPlacementType): Generator<RoomPosition>
	{
		let isRoad = false;
		const getNextLoc = function*()
		{
			switch(type)
			{
				case BuildPlacementType.RANDOM:
					while(true){yield new RoomPosition(_.random(49), _.random(49), roomName);}
				case BuildPlacementType.POPULATION_AVERAGE:
					for(const pos of generateSpiral(global.creepRosterMeta.getPositionAverage(roomName)))
					{
						if(!BuildingPlanner.isRoadPos(pos)){yield pos;}
					}
					break;
				case BuildPlacementType.GRID_ROAD:
					isRoad = true;
					/* eslint-disable-next-line no-fallthrough */
				case BuildPlacementType.GRID:
					for(const pos of generateSpiral(roomName))
					{
						if(isRoad === BuildingPlanner.isRoadPos(pos)){yield pos;}
					}
			}
		};
		const terrain = Game.rooms[roomName].getTerrain();
		for(const nextPos of getNextLoc())
		{
			if(terrain.get(nextPos.x, nextPos.y) !== TERRAIN_MASK_WALL){yield nextPos;}
		}
	};

	static isRoadPos(pos: RoomPosition): boolean
	{
		return (pos.y + pos.x) % 2 === 0;
	}

	static getPlacementTypeByStructure(type: BuildableStructureConstant): BuildPlacementType
	{
		switch(type)
		{
			default:
				return BuildPlacementType.RANDOM;
			case STRUCTURE_EXTENSION:
				return BuildPlacementType.GRID;
			case STRUCTURE_ROAD:
				return BuildPlacementType.GRID_ROAD;
			case STRUCTURE_TOWER:
				return BuildPlacementType.POPULATION_AVERAGE;
		}
	}
}