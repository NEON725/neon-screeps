import {log, LogLevel} from "utils/misc";

export enum RoomAllocation
{ /* eslint-disable-line @typescript-eslint/indent */
	NEUTRAL,
	HOSTILE,
	MILITARY,
	INDUSTRIAL,
	DIPLOMATIC,
}

export class EnergySourceIntel
{
	previousRecordedEnergy;
	unusedEnergy = 0;
	utilizationRatio = 0;
	constructor(source: Source)
	{
		this.previousRecordedEnergy = source.energy;
		this.unusedEnergy = source.energy;
	}
}

export class RoomIntel
{
	allocation: RoomAllocation | undefined = undefined;
	energySources: Record<Id<Source>, EnergySourceIntel> = {};
	averageEnergySourceUtilizationRatio = 0;
}

export default class MapIntel
{
	static run(): void
	{
		for(const roomName in Game.rooms)
		{
			const room = Game.rooms[roomName];
			const memory = room.memory;
			const allocation = MapIntel.calculateRoomAllocation(room);
			if(memory.allocation === undefined)
			{
				const myCreeps = room.find(FIND_MY_CREEPS);
				const myCreep = myCreeps.length === 1 ? myCreeps[0] : undefined;
				log(LogLevel.EVENT, "MAP", `Discovered ${roomName} (${RoomAllocation[allocation]})!`, myCreep);
			}
			else if(memory.allocation !== allocation)
			{
				log(LogLevel.EVENT, "MAP", `Room ${roomName} has been reclassified as ${RoomAllocation[allocation]}`);
			}
			memory.allocation = allocation;
			const events = room.getEventLog();
			events.forEach((eventRaw: any)=>
			{
				switch(eventRaw.event)
				{
					case EVENT_ATTACK:
						{
							log(LogLevel.WALL, "COMBAT", JSON.stringify(eventRaw));
							const event = eventRaw as EventData[EVENT_ATTACK];
							const id = event.targetId as Id<_HasId>;
							const target = Game.getObjectById(id);
							if(target !== null && "my" in target && (target as Creep).my)
							{
								log(LogLevel.DANGER, "COMBAT", `Took ${event.damage} damage!`, target as Creep);
							}
						}
						break;
					default:
						break;
				}
			});
			const sourcesIntel = memory.energySources ?? (memory.energySources = {});
			for(const source of room.find(FIND_SOURCES))
			{
				const intel = sourcesIntel[source.id] ?? (sourcesIntel[source.id] = new EnergySourceIntel(source));
				const amt = source.energy;
				if(amt > intel.previousRecordedEnergy)
				{
					intel.unusedEnergy = intel.previousRecordedEnergy;
					intel.utilizationRatio = (source.energyCapacity - intel.unusedEnergy) / source.energyCapacity;
				}
				intel.previousRecordedEnergy = amt;
			}
			const sourcesIntelList = Object.values(sourcesIntel);
			memory.averageEnergySourceUtilizationRatio = sourcesIntelList.
				map(intel => intel.utilizationRatio).
				reduce((prev, next) => prev + next, 0) /
				sourcesIntelList.length;
		}
	}

	static calculateRoomAllocation(room: Room): RoomAllocation
	{
		const controller = room.controller;
		if(controller?.my)
		{
			if(room.memory.allocation){return room.memory.allocation;}
			else
			{
				const spawns = room.find(FIND_MY_SPAWNS);
				if(spawns.length > 0){return RoomAllocation.INDUSTRIAL;}
				else{return RoomAllocation.MILITARY;}
			}
		}
		else if(controller?.owner || controller?.reservation?.username)
		{
			return RoomAllocation.HOSTILE;
		}
		const exits = Game.map.describeExits(room.name);
		const directions = Object.keys(exits) as ExitKey[];
		let adjacentRoom = false;
		for(const dir of directions)
		{
			const exit = exits[dir];
			const memory = Memory.rooms[exit || "none"];
			if(!memory){continue;}
			const{allocation, averageEnergySourceUtilizationRatio: utilization} = memory;
			if([RoomAllocation.INDUSTRIAL].includes(allocation || RoomAllocation.NEUTRAL) && utilization === 1)
			{
				adjacentRoom = true;
				break;
			}
		}
		return adjacentRoom ? RoomAllocation.MILITARY : RoomAllocation.NEUTRAL;
	}

	static findNearestRoomByType(originRoomName: string, targetType: RoomAllocation): string | null
	{
		type roomEntry = [string, RoomMemory];
		const validEntries = Object.entries(Memory.rooms).filter(([_roomName, roomMemory]: roomEntry): boolean=>roomMemory.allocation === targetType);
		if(!validEntries.length){return null;}
		const sortedEntries = _.sortBy(validEntries, ([roomName, _roomMemory]: roomEntry)=>Game.map.getRoomLinearDistance(originRoomName, roomName));
		return sortedEntries[0][0];
	}
}